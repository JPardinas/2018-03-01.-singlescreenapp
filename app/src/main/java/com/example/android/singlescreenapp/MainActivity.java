package com.example.android.singlescreenapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Booleans to know if the group schedule, days or about us info it is open
    private boolean group1 = false, group2 = false, days = false, about_us = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    //Method to open or close the about us info
    public void about_us_button(View view) {

        //Set about us preview info linear layout to change to visible or gone
        LinearLayout about_us_preview_info_Linear_Layout = (LinearLayout) findViewById(R.id.about_us_preview_info_Linear_Layout);
        //Set about us preview info TextView to change to visible or gone
        TextView about_us_all_info_Text_View = (TextView) findViewById(R.id.about_us_allinfo_Text_View);

        if (!about_us) {

            about_us_preview_info_Linear_Layout.setVisibility(View.GONE);
            about_us_all_info_Text_View.setVisibility(View.VISIBLE);
            about_us = true;
        } else {

            about_us_all_info_Text_View.setVisibility(View.GONE);
            about_us_preview_info_Linear_Layout.setVisibility(View.VISIBLE);
            about_us = false;
        }

    }


    //Method to open or close the group1 schedule info
    public void group1_Button(View view) {

        //Set arrow imageview to change it visibility
        ImageView arrow = (ImageView) findViewById(R.id.arrow1_drop_Image_view);

        //Set group1 schedule info linear layout to change to visible or gone
        LinearLayout group1_schedule_info_Linear_Layout = (LinearLayout) findViewById(R.id.group1_schedule_info_Linear_Layout);

        //Set days schedule info Linear layout to change to visible or gone
        LinearLayout schedule_days_Linear_Layout = (LinearLayout) findViewById(R.id.schedule_days_Linear_Layout);


        if (!group1) {

            arrow.setVisibility(View.INVISIBLE);
            group1_schedule_info_Linear_Layout.setVisibility(View.VISIBLE);
            schedule_days_Linear_Layout.setVisibility(View.VISIBLE);
            group1 = true;

        } else {

            if (!group2) {
                schedule_days_Linear_Layout.setVisibility(View.GONE);
            }

            arrow.setVisibility(View.VISIBLE);
            group1_schedule_info_Linear_Layout.setVisibility(View.GONE);
            group1 = false;
        }

    }


    //Method to open or close the group2 schedule info
    public void group2_Button(View view) {

        //Set arrow imageview to change it visibility
        ImageView arrow = (ImageView) findViewById(R.id.arrow2_drop_Image_view);

        //Set group2 schedule info linear layout to change to visible or gone
        LinearLayout group2_schedule_info_Linear_Layout = (LinearLayout) findViewById(R.id.group2_schedule_info_Linear_Layout);

        //Set days schedule info Linear layout to change to visible or gone
        LinearLayout schedule_days_Linear_Layout = (LinearLayout) findViewById(R.id.schedule_days_Linear_Layout);

        if (!group2) {

            arrow.setVisibility(View.INVISIBLE);
            group2_schedule_info_Linear_Layout.setVisibility(View.VISIBLE);
            schedule_days_Linear_Layout.setVisibility(View.VISIBLE);
            group2 = true;

        } else {

            if (!group1) {
                schedule_days_Linear_Layout.setVisibility(View.GONE);
            }

            arrow.setVisibility(View.VISIBLE);
            group2_schedule_info_Linear_Layout.setVisibility(View.GONE);
            group2 = false;
        }

    }


}
